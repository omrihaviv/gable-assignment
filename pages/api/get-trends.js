const handler = async (req, res) => {
  try {
    const response = await fetch("https://api.twitter.com/1.1/trends/place.json?id=1", 
    { 
      method: "GET", 
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.TWITTER_API_KEY}`,
      }
    });
    const data = await response.json();
    return res.json({trendsData: data[0]})
  } catch (err) {
    captureException(err);
  }
};

export default handler;