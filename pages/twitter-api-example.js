import Head from 'next/head'
import { useEffect, useState } from 'react';

export default function TwitterApiExample() {

  const [trendsData, setTrendsData] = useState(null);

  useEffect(async () => {
    const response = await fetch("/api/get-trends", { 
      method: "GET", 
      headers: {
      'Content-Type': 'application/json',
      }
    });
    const data = await response.json();
    setTrendsData(data.trendsData);
  }, []);

  return (
    <>
      <Head>
        <title>Trends</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>
          Trends
        </h1>

        <div>
          {trendsData?.trends.map(trend => <p key={trend.name}>{trend.name}</p>)}
        </div>
      </main>
    </>      
  )
}
